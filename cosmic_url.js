var _ = require('lodash');
var api_url = 'https://api.cosmicjs.com';
var api_version = 'v1';

exports.ObjectType = function(bucket, object){
    var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/object-type/' + object.type_slug;

    if(object.search){
        endpoint += "/search";
    }

    if(bucket.read_key){
        endpoint += '?read_key=' + bucket.read_key;
    } else {
        endpoint += '?'
    }
    if (object.limit) endpoint += '&limit=' + object.limit;
    if (object.skip) endpoint +=  '&skip=' + object.skip;


    if (object.search && object.search.metafields){
        _.each(object.search.metafields, function(v, k){
            endpoint += "&metafield_key=" + k;
            endpoint += "&metafield_value=" + v;
        });
    }

    if (object.search && object.search.objects){
        _.each(object.search.objects, function(v, k){
            endpoint += "&metafield_key=" + k;
            endpoint += "&metafield_object_slug=" + v;
        });
    }
    return endpoint;
}


