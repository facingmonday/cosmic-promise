require('es6-promise').polyfill();
require('isomorphic-fetch');

var _ = require('lodash');
var cache = require('memory-cache');
var api_url = 'https://api.cosmicjs.com';
var api_version = 'v1';
var CosmicURL = require('./cosmic_url');
var cache = require('memory-cache');

var CosmicJS = function(){
    let bucket;

    function keyMetafields(object){
        var metafields = object.metafields;
        if(metafields){
            object.metafield = _.keyBy(metafields, 'key');
        }
        return object;
    }

    return {
        init: function(config){
            bucket = config;
            return true;
        },

        test: function(callback){
            callback(false, bucket)
        },

        getBucket: function(callback){
            var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/?read_key=' + bucket.read_key;
            fetch(endpoint)
                .then(function(response){
                    if (response.status >= 400) {
                        var err = {
                            'message': 'There was an error with this request.'
                        }
                        return callback(err, false);
                    }
                    return response.json()
                })
                .then(function(response){
                    return callback(false, response);
                });
        },

        getObjects: function(callback){
            var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/objects?read_key=' + bucket.read_key;
            fetch(endpoint)
                .then(function(response){
                    if (response.status >= 400) {
                        var err = {
                            'message': 'There was an error with this request.'
                        }
                        return callback(err, false);
                    }
                    return response.json()
                })
                .then(function(response){
                    // Constructor
                    var cosmic = {};
                    var objects = response.objects;
                    cosmic.objects = {};
                    cosmic.objects.all = objects;
                    cosmic.objects.type = _.groupBy(objects, 'type_slug');
                    cosmic.object = _.map(objects, keyMetafields);
                    cosmic.object = _.keyBy(cosmic.object, 'slug');
                    return callback(false, cosmic);
                });
        },

        getObjectType: function(object){
            console.log('getObjectType', JSON.stringify(object));
            if(cache.get(JSON.stringify(object))){
                console.log('test');
                return Promise.resolve(cache.get(JSON.stringify(object)));
            } else {
                var endpoint = CosmicURL.ObjectType(bucket, object);
                console.log('Fetching: ', endpoint);
                return fetch(endpoint)
                    .then(function(response){
                        if (response.status >= 400) {
                            var err = {
                                "message" : "There was an error with this request."
                            }
                            return Promise.reject(new Error(err));
                        }
                        return response.json()
                    })
                    .then(function(response){
                        // Constructor
                        var cosmic = {};
                        var objects = response.objects;
                        // cosmic.objects = {};
                        // cosmic.objects.all = objects;
                        // cosmic.object = _.map(objects, keyMetafields);
                        // cosmic.object = _.keyBy(cosmic.object, "slug");
                        //return Promise.resolve(cosmic);
                        cache.put(JSON.stringify(object), response.objects, 60000, function(){
                            console.log('times up');
                        })
                        return Promise.resolve(objects);
                    })
                    ;
            }

        },

        getObject: function(object){
            var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/object/' + object.slug + '?read_key=' + bucket.read_key;
            if (object._id) {
                endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/object-by-id/' + object._id + '?read_key=' + bucket.read_key;
            }
            return fetch(endpoint)
                .then(function(response){
                    if (response.status >= 400) {
                        var err = {
                            'message': 'There was an error with this request.'
                        }
                        return Promise.resolve(null);
                    }
                    return response.json()
                })
                .then(function(response){
                    if(response){
                        var cosmic = {};
                        var object = response.object;
                        var metafields = object.metafields;
                        if(metafields){
                            object.metafield = _.keyBy(metafields, "key");
                        }
                        cosmic.object = object;
                        return Promise.resolve(cosmic.object);
                    } else {
                        return Promise.resolve(null);
                    }

                });
        },

        getMedia: function(callback){
            var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/media?read_key=' + bucket.read_key;
            fetch(endpoint)
                .then(function(response){
                    if (response.status >= 400) {
                        var err = {
                            'message': 'There was an error with this request.'
                        }
                        return callback(err, false);
                    }
                    return response.json()
                })
                .then(function(response){
                    return callback(false, response);
                });
        },

        addObject: function(object, callback){
            console.log('object', object);
            let endpoint, data;
            if(object.bucket){
                endpoint = api_url + '/' + api_version + '/' + object.bucket.slug + '/add-object';
                data = object.data;
            } else {
                endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/add-object';
                data = object;
            }
            return fetch(endpoint, {
                method: 'post',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(function(response){
                    if (response.status >= 400) {
                        console.log(response);
                        throw new Error(response.json())
                    }
                    return Promise.resolve(response);
                });
        },

        editObject: function(object, callback){
            var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/edit-object';
            fetch(endpoint, {
                method: 'put',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(object)
            })
                .then(function(response){
                    if (response.status >= 400) {
                        var err = {
                            'message': 'There was an error with this request.'
                        }
                        return callback(err, false);
                    }
                    return response.json()
                })
                .then(function(response){
                    return callback(false, response);
                });
        },

        deleteObject: function(object, callback){
            var endpoint = api_url + '/' + api_version + '/' + bucket.slug + '/delete-object';
            fetch(endpoint, {
                method: 'post',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(object)
            })
                .then(function(response){
                    if (response.status >= 400) {
                        var err = {
                            'message': 'There was an error with this request.'
                        }
                        return callback(err, false);
                    }
                    return response.json()
                })
                .then(function(response){
                    return callback(false, response);
                });
        }
    };
};
module.exports = new CosmicJS();
